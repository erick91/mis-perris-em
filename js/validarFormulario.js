$(function (){
$("#mi-formulario").validate({
    rules: {
      nombre: {
        required: true
      },
	  fechanacimiento: {
        required: true,
        date: true
      },
	  correo: {
        required: true,
        email: true
      },
	  rut:{
		  required:true
		  
	  },
	  region:{
	   required:true
	  },
	  ciudad:{
		  required:true
	  },
	  tipovivienda:{
		  required:true
	  }
    },
    messages: {
      nombre: {
        required: 'Ingrese un nombre',
		minlength:'Favor ingrese un nombre valido'
      },
	  fechanacimiento: {
        required: 'Ingrese una fecha de nacimiento',
		date: "ingrese una fecha valida"
      },
	  correo: {
        required: 'Ingrese un correo',
		email:"Debe ingresar un correo valido"
      },
	  rut:{
		  required:'Ingrese un rut',
		  minlength:'Favor ingresar un rut Valido'
	  },
	  ciudad:{
		  required:'Seleccione una ciudad'
	  },
	  tipovivienda:{
		  required:'Seleccione un tipo de vivienda'
	  },
	  region:{
		  required:'Seleccione una region'
	  }
	  
    }
  });
});

$("#pop").on("click", function() {
   $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});


