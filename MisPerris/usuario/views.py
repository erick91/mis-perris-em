from django.shortcuts import render, redirect, reverse
from usuario.forms import RegistrationForm#, adoptarForm
from django.contrib.auth import logout
from django.http import JsonResponse
import json
from .models import UserProfile, Mascota
from django.contrib.auth.models import User

import os

from django.http import HttpResponseRedirect, HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render, redirect

from django.http import Http404
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.core import serializers
from django.conf import settings

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('http://127.0.0.1:8000/usuario/home')
    else:
        form = RegistrationForm()
        context = {'form': form}
        return render(request, 'usuario/Formulario.html',context)
    
	
def adoptarMascota(request, pk):    
	Mascota.objects.filter(pk=pk).update(Estado='Adoptado')
	return HttpResponseRedirect(reverse('usuario:index')) 

def home(request):
	Mascotas = Mascota.objects.filter(Estado='Disponible')
	context = {'mascotas': Mascotas}
	return render(request, 'usuario/home.html', context)
	
def casa(request):
	template='usuario/home.html'
	return render(request,template)

def adoptar(request):    
        if request.method == 'POST':
                user = request.user
                us = UserProfile.objects.get(user=user)
                #form = adoptarForm(request.POST, instance=request.user)  
                us.region = request.POST["region"] 
                us.ciudad = request.POST["comunas"]
                print(request.user)
                us.save()
                        #form.save()
                        #return redirect('http://127.0.0.1:8000/usuario/home')
        #else:     
                #form = adoptarForm()
        return render(request, 'usuario/adoptar.html')#{'form': form})    
        
def serviceworker(request):
    response = render(request, app_settings.PWA_SERVICE_WORKER_PATH, content_type='application/javascript')
    return response


class crud():
	def guardarMascota(request):
		sv = Mascota(nombre=request.POST['nombre'], Descripcion=request.POST['descripcion'], image=request.FILES['image'],raza=request.POST['raza'],Estado=request.POST['estado'] )
		sv.save()
		return HttpResponseRedirect(reverse('usuario:index'))

	def editarMascota(request, data_id):
		data = get_object_or_404(Mascota, pk=data_id)
		alldata = Mascota.objects.order_by('-nombre') 
		return render(request, 'usuario/home2.html', {'data': data, 'qid' : data_id, 'getdata' : alldata})

	def actualizarMascota(request):
		ed = Mascota.objects.get(id=request.POST['qid'])
		
		imagePath = os.path.join(settings.MEDIA_ROOT, ed.image.name)

		if 'image' in request.FILES:
			ed.image = request.FILES['image']
			if os.path.isfile(imagePath):
				os.remove(imagePath)

	
		ed.nombre=request.POST['nombre']
		ed.raza=request.POST['raza']

		ed.Descripcion=request.POST['descripcion']
		
		ed.save()

		return HttpResponseRedirect(reverse('usuario:index'))

	def eliminarMascota(request, data_id):
		dl = Mascota.objects.get(id=data_id)

		imagePath = os.path.join(settings.MEDIA_ROOT, dl.image.name)
		

		if os.path.isfile(imagePath):
			os.remove(imagePath)

		

		dl.delete()

		return HttpResponseRedirect(reverse('usuario:index'))
        #def getimage(request):
                

class IndexView(generic.ListView):
	template_name = 'usuario/home2.html'
	context_object_name = 'latest_question_list'

	def get_queryset(self):
		return Mascota.objects.order_by('-nombre')
	