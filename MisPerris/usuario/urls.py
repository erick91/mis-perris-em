from django.urls import path, re_path,include
from django.conf.urls import url
from . import views
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth import views as auth_views 
from django.views.generic import TemplateView

app_name = 'usuario'
urlpatterns = [
    path('', include('pwa.urls')),
    url(r'^mantenedor/$', views.IndexView.as_view(), name='index'),
    path('registrar', views.index, name='lacasa'),
    path('login/', LoginView.as_view(template_name="usuario/login.html")),
    path('', views.home, name='home'),
    path('logout/', LogoutView.as_view(template_name="usuario/home.html")),
    path('adoptar', views.adoptar, name='adoptar'),
    url(r'^guardar_mascota/$', views.crud.guardarMascota, name='guardar_mascota'),
    url(r'^actualizar_mascota/$', views.crud.actualizarMascota, name='actualizar_mascota'),
    url(r'^actualizar_mascota/(?P<pk>\d+)/', views.adoptarMascota, name='adoptar_mascota'),
    url(r'^([0-9]+)/editar_mascota/$', views.crud.editarMascota, name='editar_mascota'),
    url(r'^([0-9]+)/eliminar_mascota/$', views.crud.eliminarMascota, name='eliminar_mascota'),
    url(r'^password-reset/$', auth_views.PasswordResetView.as_view(template_name="usuario/password_reset.html")),
    url(r'^password-reset/done/$', auth_views.PasswordResetDoneView.as_view(template_name="usuario/password_reset_done.html")),
    url(r'^password-reset/confirm/(?P<uidb64>[\w-]+)/(?P<token>[\w-]+)/$', auth_views.PasswordResetConfirmView.as_view(template_name="usuario/password_reset_confirm.html")),
    url(r'serviceworker.js',  (TemplateView.as_view(template_name="serviceworker.js", content_type='application/javascript',)), name='serviceworker.js'),
]
