# Generated by Django 2.1.2 on 2018-11-05 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0002_auto_20181104_1914'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mascota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(blank='true', max_length=200)),
                ('raza', models.CharField(max_length=15)),
                ('Descripcion', models.CharField(max_length=200)),
                ('Estado', models.CharField(max_length=200)),
                ('image', models.FileField(blank='true', upload_to='images')),
            ],
        ),
    ]
