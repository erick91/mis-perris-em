from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    ciudad = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    
    def __str__(self):
        return self.user.username

def create_profile(sender , **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile, sender=User)

class Mascota(models.Model):
    nombre = models.CharField(max_length=200, blank='true')
    raza = models.CharField(max_length=15)
    Descripcion = models.CharField(max_length=200)
    Estado = models.CharField(max_length=200)
    image = models.ImageField(upload_to='mascota', blank='true')

    def __str__(self):
        return self.Descripcion
    def get_absolute_url(self): 
    	return self.image

class Choice(models.Model):
    question = models.ForeignKey(Mascota, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text