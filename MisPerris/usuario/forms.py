from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import UserProfile

class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        )
        labels = {
            'username':'Usuario',
            'first_name':'Nombres',
            'last_name':'Apellidos',
            'email':'Correo Electronico'
        }
    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user

#class adoptarForm(forms.ModelForm):        
#        class Meta:
#            model = UserProfile
#            fields = ('ciudad','region')
#            
#        def save(self, user=None):
#            user_profile = super(adoptarForm, self).save(commit=False)
#            user_profile.ciudad = self.cleaned_data['ciudad']
#            user_profile.region = self.cleaned_data['region']
#            if user:
#                user_profile.user = user
#            user_profile.save()
#            return user_profile

