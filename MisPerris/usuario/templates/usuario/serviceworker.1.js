var CACHE_NAME = 'Wooooooo';
var urlsToCache = [
        "/",
        '/static/serviceworker.js',
        '/static/css/style.css',
      
        '/static/Google-Plus.png',
        '/static/logoface.png',
        '/static/logoinstagram.png',
        '/static/Mail.png',
        '/static/rescatados/Bigotes.jpg',
        '/static/rescatados/Chocolate.jpg',
        '/static/rescatados/Luna.jpg',
        '/static/rescatados/Maya.jpg',
        '/static/rescatados/Oso.jpg',
        '/static/rescatados/Pexel.jpg',
        '/static/rescatados/Wifi.jpg',
        '/static/adoptados/Apolo.jpg',
        '/static/adoptados/Duque.jpg',
        '/static/adoptados/Tom.jpg',

        'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
];


self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
      caches.open(CACHE_NAME)
        .then(function(cache) {
          console.log('Opened cache');
          return cache.addAll(urlsToCache);
        })
    );
  });
  
  self.addEventListener('fetch', function(event){
      event.respondWith(
          caches.match(event.request).then(function(response) {
              if(response) {
                  return response;
              }
  
              return fetch(event.request);
          })
      );
  });

